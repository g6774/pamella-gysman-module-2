// ignore_for_file: unused_import

import 'package:flutter/material.dart';

void main() {
  //Print list of winners alphabetically and chronologiically
  var winner2021 = WinnersList2021();
  winner2021.sortWinners();
  winner2021.countWinners10();
  var winner2020 = WinnersList2020();
  winner2020.sortWinners();
  winner2020.countWinners9();
  var winner2019 = WinnersList2019();
  winner2019.sortWinners();
  winner2019.countWinners8();
  var winner2018 = WinnersList2018();
  winner2018.sortWinners();
  winner2018.countWinners7();
  var winner2017 = WinnersList2017();
  winner2017.sortWinners();
  winner2017.countWinners6();
  var winner2016 = WinnersList2016();
  winner2016.sortWinners();
  winner2016.countWinners5();
  var winner2015 = WinnersList2015();
  winner2015.sortWinners();
  winner2015.countWinners4();
  var winner2014 = WinnersList2014();
  winner2014.sortWinners();
  winner2014.countWinners3();
  var winner2013 = WinnersList2013();
  winner2013.sortWinners();
  winner2013.countWinners2();
  var winner2012 = WinnersList2012();
  winner2012.sortWinners();
  winner2012.countWinners1();

//Print winning app for 2017 and 2018
  print('App of the Year');
  print('2017: Shyft');
  print('2018: Naked Insurance');
}

//Lists of MTN Business App winners since 2012
//2021 winners
class WinnersList2021 {
  List<String> winners10 = [
    '2021 winners:',
    'iiDENTIFii app',
    'Hellopay SoftPOS',
    'Guardian Health Platform',
    'Ambani Africa',
    'Murimi',
    'Shyft',
    'Sisa',
    'UniWise',
    'Kazi',
    'Takealot app',
    'Rekindle Learning app',
    'Roadsave',
    'AfrihostasyEquities',
    'Examsta',
    'Checkers Sixty60',
    'Technishen',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Xitsonga Dictionary',
    'StokFella',
    'Bottles',
    'Matric Live',
    'Guardian Health',
    'Checkers Sixty60',
    'My Pregnancy Journey'
  ];
  sortWinners() {
    winners10.sort((item1, item2) => item1.compareTo(item2));
    print(winners10);
  }

  countWinners10() {
    var count10 = (winners10.length);
    print('Total Winners = $count10');
    print("\n");
  }
}

//2020 winners
class WinnersList2020 {
  List<String> winners9 = [
    '2020 Winners:',
    'EasyEquities',
    'Examsta',
    'Checkers Sixty60',
    'Technishen',
    'BirdPro',
    'Lexie Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Xitsonga Dictionary',
    'StokFella',
    'Bottles',
    'Matric Live',
    'Guardian Health',
    'My Pregnancy Journey'
  ];
  sortWinners() {
    winners9.sort((item1, item2) => item1.compareTo(item2));
    print(winners9);
  }

  countWinners9() {
    var count9 = (winners9.length);
    print('Total Winners = $count9');
    print("\n");
  }
}

//2019 winners
class WinnersList2019 {
  List<String> winners8 = [
    '2019 Winners:',
    'SI Realities',
    'Lost Defence',
    'Franc',
    'Vula Mobile',
    'Matric Live',
    'My Pregnancy Journal',
    'LocTransie',
    'Hydra',
    'Bottles',
    'Over',
    'Digger',
    'Mo Wash'
  ];
  sortWinners() {
    winners8.sort((item1, item2) => item1.compareTo(item2));
    print(winners8);
  }

  countWinners8() {
    var count8 = (winners8.length);
    print('Total Winners = $count8');
    print("\n");
  }
}

//2018 winners
class WinnersList2018 {
  List<String> winners7 = [
    '2018 Winners:',
    'Pineapple',
    'Cowa Bunga',
    'Digemy Knowledge Partner',
    'Besmarter',
    'Bestee',
    'The African Cyber Gaming League App (ACGL)',
    'dbTrack',
    'Stokfella',
    'Difela Hymns',
    'Xander English 1-20',
    'Ctrl',
    'Khula',
    'ASI Snakes'
  ];
  sortWinners() {
    winners7.sort((item1, item2) => item1.compareTo(item2));
    print(winners7);
  }

  countWinners7() {
    var count7 = (winners7.length);
    print('Total Winners = $count7');
    print("\n");
  }
}

//2017 winners
class WinnersList2017 {
  List<String> winners6 = [
    '2017 Winners:',
    'TransUnion 1Check',
    'OrderIN',
    'InterGreatMe',
    'EcoSlips',
    'Zulzi',
    'Hey Jude',
    'The ORU Social',
    'TouchSA',
    'Pick n Pay Super Animals 2',
    'The TreeApp South Africa',
    'WatIf Health Portal',
    'Awethu Project',
    'Shyft for Standard Bank'
  ];
  sortWinners() {
    winners6.sort((item1, item2) => item1.compareTo(item2));
    print(winners6);
  }

  countWinners6() {
    var count6 = (winners6.length);
    print('Total Winners = $count6');
    print("\n");
  }
}

//2016 winners
class WinnersList2016 {
  List<String> winners5 = [
    '2016 Winners:',
    'iKhoha',
    'HearZA',
    'Tuta-me',
    'KaChing',
    'Friendly Math Monsters for Kindergarten',
    'Domestly'
  ];
  sortWinners() {
    winners5.sort((item1, item2) => item1.compareTo(item2));
    print(winners5);
  }

  countWinners5() {
    var count5 = (winners5.length);
    print('Total Winners = $count5');
    print("\n");
  }
}

//2015 winners
class WinnersList2015 {
  List<String> winners4 = [
    '2015 Winners:',
    'VulaMobile',
    'DStv Now',
    'WumDrop',
    'CPUT Mobile',
    'EskomSePush',
    'M4JAM'
  ];
  sortWinners() {
    winners4.sort((item1, item2) => item1.compareTo(item2));
    print(winners4);
  }

  countWinners4() {
    var count4 = (winners4.length);
    print('Total Winners = $count4');
    print("\n");
  }
}

//2014 winners
class WinnersList2014 {
  List<String> winners3 = [
    '2014 Winners:',
    'Sync Mobile',
    'SuperSport',
    'MyBelongings',
    'VIGO',
    'Rea Vaya',
    'Zapper',
    'Wildlife Tracker',
    'Zapper',
    'Live Inspect'
  ];
  sortWinners() {
    winners3.sort((item1, item2) => item1.compareTo(item2));
    print(winners3);
  }

  countWinners3() {
    var count3 = (winners3.length);
    print('Total Winners = $count3');
    print("\n");
  }
}

//2013 winners
class WinnersList2013 {
  List<String> winners2 = [
    '2013 Winners:',
    'DStv',
    '.comm Telco Data Visualizer',
    'PriceCheck Mobile',
    'MarkitShare',
    'Nedbank App Suite',
    'SnapScan',
    'Kids Aid bookly',
    'Gautrain Buddy'
  ];
  sortWinners() {
    winners2.sort((item1, item2) => item1.compareTo(item2));
    print(winners2);
  }

  countWinners2() {
    var count2 = (winners2.length);
    print('Total Winners = $count2');
    print("\n");
  }
}

//2012 winners
class WinnersList2012 {
  List winners1 = [
    '2012 Winners:',
    'FNB Banking',
    'Health ID',
    'TransUnion Dealer Guide',
    'Rapidtargets',
    'Matchy',
    'Plascon Inspire Me',
    'PhraZApp'
  ];
  sortWinners() {
    winners1.sort((item1, item2) => item1.compareTo(item2));
    print(winners1);
  }

  countWinners1() {
    var count1 = (winners1.length);
    print('Total Winners = $count1');
    print("\n");
  }
}
